﻿using PaPaPa.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Models.Datings
{
    /// <summary>
    /// 约会信息
    /// </summary>
    public class DatingInfo : EntityBase<int>
    {
        /// <summary>
        /// 约会名称
        /// </summary>
        [Required, MaxLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// 约会时间
        /// </summary>
        public DateTime DatingTime { get; set; }

        /// <summary>
        /// 约会地址
        /// </summary>
        [Required, MaxLength(200)]
        public string Address { get; set; }

        /// <summary>
        /// 约会对象
        /// </summary>
        [Required, MaxLength(20)]
        public string DatingUser { get; set; }

        /// <summary>
        /// 约会描述
        /// </summary>
        [Required, MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ViewTimes { get; set; }

        /// <summary>
        /// 约会类型
        /// </summary>
        public DatingType DatingType { get; set; }

        public DatingInfo()
        {
            //todo 邀约环节再解决约会参与人问题
            DatingUser = "test";
        }
    }
}
