﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Models.Datings
{
    /// <summary>
    /// 约会信息视图模型
    /// </summary>
    public class DatingInfoViewModel
    {
        public int Id { get; set; }

        /// <summary>
        /// 约会名称
        /// </summary>
        [Display(Name = "约会名称")]
        public string Name { get; set; }

        /// <summary>
        /// 约会时间
        /// </summary>
        [Display(Name = "约会时间")]
        public DateTime DatingTime { get; set; }

        /// <summary>
        /// 约会地址
        /// </summary>
        [Display(Name = "约会地址")]
        public string Address { get; set; }

        /// <summary>
        /// 约会对象
        /// </summary>
        [Display(Name = "约会对象")]
        public List<int> InviteUser { get; set; }
        /// <summary>
        /// 约会描述
        /// </summary>
        [Display(Name = "约会描述")]
        public string Description { get; set; }

        /// <summary>
        /// 约会类型
        /// </summary>
        [Display(Name = "约会类型")]
        public DatingType DatingType { get; set; }
    }
}
